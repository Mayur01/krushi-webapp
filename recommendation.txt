Rice;Kharif;Clay
Wheat;Rabi;Murmur
Dry chillies;Total;Red/Black
Dry ginger;Total;Red
Oilseeds;Total;Red/Black
Potato;Total;Loose/Loam
Pulses;Total;Clay
Sugarcane;Total;Black/Loam
Turmeric;Total;Clay
Maize;Kharif;Black
Cotton;Kharif;Black
Groundnut;Kharif;Sany/Loose
Jowar;Kharif;Red
Bajra;Kharif;Black
Peas;Rabi;Black
Gram;Rabi;Loam
Barley;Rabi;Clay
Red Chillies;Kharif;Red/Black
Mustard;Rabi;Clay
Soyabean;Kharif;Loam/Clay
Sunflower;Kharif;Clay