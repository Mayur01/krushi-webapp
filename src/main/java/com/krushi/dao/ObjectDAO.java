package com.krushi.dao;

import java.io.Serializable;

public interface ObjectDAO {

    public <T> T getObjectByParam(Class<T> entity,String param,Object obj);
	
	public <T> T getObjectById(Class<T> entity,Serializable id);
	
	public Object saveObject(Object entity);
	
	public <T> T listObject(Class<T> entity, String id);
	
	
}
