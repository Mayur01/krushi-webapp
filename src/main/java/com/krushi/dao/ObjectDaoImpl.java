package com.krushi.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("ObjectDAO")
public class ObjectDaoImpl implements ObjectDAO{

	private SessionFactory sessionFactory;
	
	@Autowired
	private EntityManager entityManager;
	
	
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void persist(Object entity) {
		getSession().persist(entity);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T getObjectByParam(Class<T> entity, String param, Object paramValue) {
		Query q = entityManager.createQuery("SELECT e FROM "+ entity.getName() + " e WHERE "+param+" = :value ORDER BY e.id DESC");
			q.setParameter("value", paramValue);
			q.setMaxResults(1);
			try {
				return (T) q.getSingleResult();
			} catch (NoResultException exc) {
				return null;
			}
	}

	
	@Override
	public <T> T getObjectById(Class<T> entity, Serializable id) {
		return entityManager.getReference(entity, id);
	}


	@Override
	public Object saveObject(Object entity)  {
		return entityManager.merge(entity);
	}
	
	@Override
	public <T> T listObject(Class<T> entity, String id) {
		Query q = entityManager.createQuery(
				"SELECT e FROM " + entity.getName() + " e ");
		return (T) q.getResultList();
	}
}
