package com.krushi.recommendation;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.json.JSONObject;

import com.krushi.dto.CropDTO;
import com.krushi.model.CropModel;

public class RecommendationUtility {
	
	Mapper mapper = new DozerBeanMapper();
	public static HashMap<String, JSONObject> crops = new HashMap<String, JSONObject>();
	
	 public void readFile() {
		 BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/recommendation.txt")));
	        try {
	            //iterate through all lines in file
	          /*  while (n.hasNextLine()) {
	                String line = n.nextLine();
	                //check if line is correctly formatted, and if so, call createObjects with given line
	                if (line.length() > 0) {
	                    createObjects(line);
	                }
	            }*/
	        	String contentLine = br.readLine();
	     	   while (contentLine != null) {
	     	      System.out.println(contentLine);
	     	      contentLine = br.readLine();
	     	     if (contentLine.length() > 1) {
	     	    	 	createObjects(contentLine);
	     	     }
	     	   }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	 
	 public void createObjects(String bookInfo) {
		 try {
			 String[] info = bookInfo.split(";");
		        String name = info[0];
		        String season = info[1];
		        String soil = info[2];

		        CropModel cropModel = new CropModel(name,season,soil);
		        CropDTO cropDTO = mapper.map(cropModel, CropDTO.class);
		        //System.out.println("cropModel " + cropModel);
		        //System.out.println("cropDTO " + cropDTO);
		        //System.out.println("objectDao " + objectDao);
		        //System.out.println("name " + name);
		        //System.out.println("crops First " + crops);
		        JSONObject obj = new JSONObject();
		        obj.put("name", name);
		        obj.put("season", season);
		        obj.put("soil", soil);
		        crops.put(name, obj);
		        //this.setCrops(crops);
		        //System.out.println("crops First " + crops);
		        //objectDao().saveObject(cropDTO);	
		 }catch (Exception e) {
	            e.printStackTrace();
	        }
	 }

	public HashMap<String, JSONObject> getCrops() {
		return crops;
	}

	public void setCrops(HashMap<String, JSONObject> crops) {
		this.crops = crops;
	}
}
