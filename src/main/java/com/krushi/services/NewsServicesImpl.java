package com.krushi.services;

import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.krushi.dao.ObjectDAO;
import com.krushi.dto.NewsDTO;
import com.krushi.dto.UserDTO;
import com.krushi.model.NewsModel;
import com.krushi.model.ResponseModel;

@Service
public class NewsServicesImpl implements NewsService{
	
	@Autowired
	private ObjectDAO objectDao;

	Mapper mapper = new DozerBeanMapper();
	
	@Override
	public ResponseModel saveNews(NewsModel newsModel) {
		ResponseModel responseModel = ResponseModel.getInstance();
		try{
			NewsDTO newsDTO = mapper.map(newsModel, NewsDTO.class);
			objectDao.saveObject(newsDTO);
			responseModel.setObject(newsDTO);
			responseModel.setStatus(HttpStatus.OK.toString());
			responseModel.setMessage("News saved successfully..");
		}catch(Exception e){
			responseModel.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			responseModel.setError(e.getMessage());
		}
		return responseModel;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResponseModel getNews() {
		ResponseModel responseModel = ResponseModel.getInstance();
		try{
			List<NewsDTO> newsList = (List<NewsDTO>) objectDao.listObject(NewsDTO.class, "id");
			responseModel.setObject(newsList);
			responseModel.setStatus(HttpStatus.OK.toString());
			responseModel.setMessage("News List..");
		}catch(Exception e){
			
		}
		return responseModel;
	}

	
}
