package com.krushi.services;

import com.krushi.exception.CustomException;
import com.krushi.model.ResponseModel;
import com.krushi.model.WeatherModel;

public interface WeatherService {
	public ResponseModel weather(WeatherModel weatherModel) throws CustomException;
}
