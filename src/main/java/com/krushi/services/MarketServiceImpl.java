package com.krushi.services;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.krushi.exception.CustomException;
import com.krushi.model.MarketModel;
import com.krushi.model.ResponseModel;

@Service
public class MarketServiceImpl implements MarketService{

	@Override
	public ResponseModel getMarketRate(MarketModel marketModel) throws CustomException {
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			
			//https://api.data.gov.in/resource/9ef84268-d588-465a-a308-a864a43d0070?api-key=579b464db66ec23bdd000001cdd3946e44ce4aad7209ff7b23ac571b&format=json&offset=0&limit=50&filters[market]=Pune
			String marketUrl = "https://api.data.gov.in/resource/9ef84268-d588-465a-a308-a864a43d0070?api-key=579b464db66ec23bdd000001cdd3946e44ce4aad7209ff7b23ac571b&format=json&offset=0&limit=50&filters[market]=";
			marketUrl = marketUrl + marketModel.getMarket();
			RestTemplate restTemplate = new RestTemplate();
			String val = restTemplate.getForObject(marketUrl, String.class);
			JSONObject marketObject = new JSONObject(val);
			responseModel.setObject(marketObject.toString());
			responseModel.setMessage("Market rate fetch successfully..");
		} catch (Exception e) {
			responseModel.setMessage("Market rate fetching failed..");
			responseModel.setStatus(HttpStatus.BAD_REQUEST.toString());
		}
		return responseModel;
	}
}
