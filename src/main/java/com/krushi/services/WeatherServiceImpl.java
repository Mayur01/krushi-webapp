package com.krushi.services;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.krushi.exception.CustomException;
import com.krushi.model.ResponseModel;
import com.krushi.model.WeatherModel;

@Service
public class WeatherServiceImpl implements WeatherService {

	@Override
	public ResponseModel weather(WeatherModel weatherModel) throws CustomException {
		RestTemplate restTemplate = new RestTemplate();
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			String weatherUrl = "http://api.openweathermap.org/data/2.5/weather?APPID=7fa7f9a82cea2c43fb171a72b8d3adf8&units=metric&lon=";
			weatherUrl = weatherUrl + weatherModel.getLongitude() +"&lat=" + weatherModel.getLatitude();
			String str = restTemplate.getForObject(weatherUrl, String.class);
			Object weatherObj = new JSONObject(str);
			//http://api.openweathermap.org/data/2.5/weather?APPID=7fa7f9a82cea2c43fb171a72b8d3adf8&units=metric&lon=73.85&lat=18.52
			//http://api.openweathermap.org/data/2.5/weather?APPID=7fa7f9a82cea2c43fb171a72b8d3adf8&units=metric&q=Pune,India
			responseModel.setMessage("Weather details fetch successfully..");
			responseModel.setObject(weatherObj.toString());
		} catch (JSONException e) {
			responseModel.setMessage("Weather details fetching failed..");
			responseModel.setStatus(HttpStatus.BAD_REQUEST.toString());
		}
	    return responseModel;
	}

	
}
