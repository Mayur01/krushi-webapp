package com.krushi.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.krushi.dao.ObjectDAO;
import com.krushi.dto.ShivarDTO;
import com.krushi.dto.UserDTO;
import com.krushi.exception.CustomException;
import com.krushi.model.ResponseModel;
import com.krushi.model.ShivarModel;

@Service
public class ShivarServiceImpl implements ShivarService{

	@Autowired
	private ObjectDAO objectDao;
	
	Mapper mapper = new DozerBeanMapper();
	
	@Override
	public ResponseModel getShivar(Integer userId) throws CustomException {
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			UserDTO userDto = objectDao.getObjectById(UserDTO.class, userId);
			ShivarDTO shivarDto = objectDao.getObjectByParam(ShivarDTO.class, "user", userDto);
			responseModel.setMessage("Shivar fetching done successfully..");
			responseModel.setStatus(HttpStatus.OK.toString());
			responseModel.setObject(shivarDto);
		}catch(Exception e) {
			responseModel.setMessage("Shivar details fetching failed..");
			responseModel.setStatus(HttpStatus.BAD_REQUEST.toString());
		}
		return responseModel;
	}

	@Override
	public ResponseModel saveShivar(ShivarModel shivarModel) throws CustomException {
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			ShivarDTO shivarDTO = mapper.map(shivarModel, ShivarDTO.class);
			UserDTO userDto = objectDao.getObjectById(UserDTO.class,shivarModel.getUserId());
			shivarDTO.setUser(userDto);
			ShivarDTO ss =(ShivarDTO) objectDao.saveObject(shivarDTO);
			responseModel.setObject(ss);
			responseModel.setMessage("Shivar details stored successfully..");
			responseModel.setStatus(HttpStatus.OK.toString());
			
		}catch(Exception e) {
			e.printStackTrace();
			responseModel.setMessage("Shivar details storing failed..");
			responseModel.setStatus(HttpStatus.BAD_REQUEST.toString());
		}
		return responseModel;
	}

	@Override
	public ResponseModel getTahsil() throws CustomException {
		ResponseModel responseModel = ResponseModel.getInstance();
		Map<String, List<String>> tahsilList = new HashMap<String, List<String>>();
		List<String> tahsil = new ArrayList<String>();
		tahsil.add("aaa");
		tahsil.add("bbb");
		tahsilList.put("ahamadnagr", tahsil);
		responseModel.setObject(tahsilList);
		return responseModel;
	}
}