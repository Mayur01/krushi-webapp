package com.krushi.services;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.krushi.dao.ObjectDAO;
import com.krushi.dto.UserDTO;
import com.krushi.exception.CustomException;
import com.krushi.model.ResponseModel;
import com.krushi.model.UserModel;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private ObjectDAO objectDao;
	
	Mapper mapper = new DozerBeanMapper();
	
	@Override
	public ResponseModel login(UserModel userModel) throws CustomException{
		ResponseModel responseModel = ResponseModel.getInstance();
		
		try {
			if(null != userModel.getMobile() && null !=userModel.getPassword() &&
					"" != (userModel.getMobile().toString()) && "" !=userModel.getPassword()) {
				UserDTO userDto = objectDao.getObjectByParam(UserDTO.class, "mobile", userModel.getMobile());
				if(null != userDto) {
					if(userDto.getPassword().equals(userModel.getPassword())) {
						responseModel.setMessage("Login Successfully..");
						responseModel.setObject(userDto);
						responseModel.setStatus(HttpStatus.OK.toString());
					}else {
						responseModel.setMessage("Incorret password has been entered..");
						responseModel.setStatus(HttpStatus.NON_AUTHORITATIVE_INFORMATION.toString());
						//throw new CustomException("Incorrect password has been entered");
					}
				}else {
					responseModel.setStatus(HttpStatus.UNAUTHORIZED.toString());
					responseModel.setMessage("User is not registerd.Please register first..");
					//throw new CustomException("User is not registerd.Please register first..");
				}
			}else {
				responseModel.setStatus(HttpStatus.NOT_FOUND.toString());
				responseModel.setMessage("Mobile no/Password is empty..");
				//throw new CustomException("Mobile no/Password is empty");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}

	@Override
	public ResponseModel register(UserModel userModel) {
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			UserDTO userDTO2 = objectDao.getObjectByParam(UserDTO.class, "mobile", userModel.getMobile());
			System.out.println("userDTO2 " + userDTO2);
			if(userDTO2 == null) {
				UserDTO userDTO1 = mapper.map(userModel, UserDTO.class);
				responseModel.setMessage("Register Successfully..");
				responseModel.setStatus(HttpStatus.OK.toString());
				UserDTO user = (UserDTO) objectDao.saveObject(userDTO1);
				responseModel.setObject(user);
			}else {
				responseModel.setMessage("Already Registered..");
				responseModel.setStatus(HttpStatus.BAD_REQUEST.toString());
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return responseModel;
		
	}
	
	@Override
	public ResponseModel forgotPassword(UserModel userModel) {
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			UserDTO userDTO1 = mapper.map(userModel, UserDTO.class);
			UserDTO userDTO2 = objectDao.getObjectByParam(UserDTO.class, "mobile", userModel.getMobile());
			if(userDTO1.getMobile() != userDTO2.getMobile()) {
				userDTO2.setPassword(userDTO1.getPassword());
				responseModel.setObject(userDTO2);
				responseModel.setMessage("New password set successfully..");
				responseModel.setStatus(HttpStatus.OK.toString());
				objectDao.saveObject(userDTO2);
			}else {
				responseModel.setMessage("New password not set..");
				responseModel.setStatus(HttpStatus.BAD_REQUEST.toString());
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return responseModel;
		
	}
	
	@Override
	public ResponseModel changePassword(UserModel userModel) {
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			UserDTO userDTO = objectDao.getObjectByParam(UserDTO.class, "mobile", userModel.getMobile());
			System.out.println("EEE222 " + userDTO.getPassword());
			if(userModel.getMobile().equals(userDTO.getMobile()) && userModel.getOldPassword().equals(userDTO.getPassword())) {
				System.out.println("EEE " + userModel.getNewPassword());
				userDTO.setPassword(userModel.getNewPassword());
				responseModel.setObject(userDTO);
				responseModel.setMessage("Password changed successfully..");
				responseModel.setStatus(HttpStatus.OK.toString());
				objectDao.saveObject(userDTO);
			}else {
				responseModel.setMessage("Please enter correct mobile no/old password..");
				responseModel.setStatus(HttpStatus.BAD_REQUEST.toString());
			}
		}catch(Exception e) {
		}
		return responseModel;
		
	}
}
