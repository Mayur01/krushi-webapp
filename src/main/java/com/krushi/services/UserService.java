package com.krushi.services;


import com.krushi.exception.CustomException;
import com.krushi.model.ResponseModel;
import com.krushi.model.UserModel;

public interface UserService {

	public ResponseModel login(UserModel userModel) throws CustomException;
	public ResponseModel register(UserModel userModel) throws CustomException;
	public ResponseModel forgotPassword(UserModel userModel) throws CustomException;
	public ResponseModel changePassword(UserModel userModel) throws CustomException;
}


