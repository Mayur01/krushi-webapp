package com.krushi.services;

import com.krushi.model.NewsModel;
import com.krushi.model.ResponseModel;

public interface NewsService {

	public ResponseModel saveNews(NewsModel newsModel);
	public ResponseModel getNews();
}
