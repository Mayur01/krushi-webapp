package com.krushi.services;

import com.krushi.exception.CustomException;
import com.krushi.model.MarketModel;
import com.krushi.model.ResponseModel;

public interface MarketService {

	public ResponseModel getMarketRate(MarketModel marketModel) throws CustomException;
}
