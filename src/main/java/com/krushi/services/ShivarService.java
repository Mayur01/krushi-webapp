package com.krushi.services;

import com.krushi.exception.CustomException;
import com.krushi.model.ResponseModel;
import com.krushi.model.ShivarModel;

public interface ShivarService {

	public ResponseModel getShivar(Integer userId) throws CustomException;
	public ResponseModel saveShivar(ShivarModel shivarModel) throws CustomException;
	public ResponseModel getTahsil()throws CustomException;
	
}
