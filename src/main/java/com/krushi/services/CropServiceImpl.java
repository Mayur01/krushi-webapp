package com.krushi.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.krushi.exception.CustomException;
import com.krushi.model.CropModel;
import com.krushi.model.ResponseModel;
import com.krushi.recommendation.RecommendationUtility;

@Service
public class CropServiceImpl implements CropService{

	@Override
	public ResponseModel getCropRecommendation(CropModel cropModel) throws CustomException {
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			responseModel = recommendationByKNN(cropModel);
			responseModel.setMessage("Crop recommendation done Successfully..");
			responseModel.setStatus(HttpStatus.OK.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}
	
	@SuppressWarnings("unchecked")
	public ResponseModel recommendationByKNN(CropModel cropModel) {
		
		ResponseModel responseModel = ResponseModel.getInstance();
		SortedMap<Integer, List<JSONObject>> sortedCrop = new TreeMap<Integer, List<JSONObject>>();
		try {
			HashMap<String, JSONObject> crops = new RecommendationUtility().getCrops();
			System.out.println("crops Second " + crops);
			for (String name : crops.keySet()) {
				int count = 0;
				JSONObject value = crops.get(name);
				
				if(value.getString("season").contains(cropModel.getSeason())) 
					count++;
				if(value.getString("soil").contains(cropModel.getSoil())) 
					count++;
				
				if(count > 0) {
					List<JSONObject> oldCrop = sortedCrop.get(count);
					if(!(oldCrop == null)) {
						oldCrop.add(value);
					}else {
						oldCrop = new ArrayList<JSONObject>();
						oldCrop.add(value);
					}
					sortedCrop.put(count, oldCrop);
				}
			}
			JSONArray cropDtoArray = new JSONArray();
			NavigableSet<Integer> navig = ((TreeMap)sortedCrop ).descendingKeySet();
			for (Iterator<Integer> iter = navig.iterator();iter.hasNext();) {
			    Integer key = iter.next();
				List<JSONObject> list = sortedCrop.get(key);
				for(int i=0; i<list.size(); i++) {
					JSONObject cropDTO  = list.get(i);
					if(cropDtoArray.length() <= 4)
						cropDtoArray.put((JSONObject) cropDTO);
					else {
						break;
					}
				}
			}
			JSONObject cropDtoObj = new JSONObject();
			cropDtoObj.put("RcommendedCrops",cropDtoArray);
			responseModel.setObject(cropDtoObj.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}
}
