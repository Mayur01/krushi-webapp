package com.krushi.services;

import com.krushi.exception.CustomException;
import com.krushi.model.CropModel;
import com.krushi.model.ResponseModel;

public interface CropService {

	public ResponseModel getCropRecommendation(CropModel cropModel) throws CustomException;
}
