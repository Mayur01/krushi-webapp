package com.krushi.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "shivar")
public class ShivarDTO implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id",unique = true ,nullable = false)
	private Integer id;
	
	@Column(name = "water_source")
	private String waterSource;
	
	@Column(name = "area")
	private String area;
	
	@Column(name = "soil_Type")
	private String soilType;
	
	@Column(name = "sinchan_type")
	private String sinchanType;
	
	@Column(name = "side_Business")
	private String sideBusiness;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private UserDTO user;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getWaterSource() {
		return waterSource;
	}

	public void setWaterSource(String waterSource) {
		this.waterSource = waterSource;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getSoilType() {
		return soilType;
	}

	public void setSoilType(String soilType) {
		this.soilType = soilType;
	}

	public String getSinchanType() {
		return sinchanType;
	}

	public void setSinchanType(String sinchanType) {
		this.sinchanType = sinchanType;
	}

	public String getSideBusiness() {
		return sideBusiness;
	}

	public void setSideBusiness(String sideBusiness) {
		this.sideBusiness = sideBusiness;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}
	
	
	

}
