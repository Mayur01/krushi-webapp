package com.krushi.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.json.JSONObject;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "crops")
@EntityListeners(AuditingEntityListener.class)
public class CropDTO {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	 @Column(name = "crop_name", unique = true, nullable = false)
	 private String name;

	 @Column(name = "season", nullable = false)
	 private String season;

	 @Column(name = "soil_type", nullable = false)
	 private String soil;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getSoil() {
		return soil;
	}

	public void setSoil(String soil) {
		this.soil = soil;
	}
	 
	 @Override
	 public String toString() {
		 JSONObject response = new JSONObject();
		 try{
			 response.put("name", getName());
			 response.put("season", getSeason());
			 response.put("soil", getSoil());
		 }catch (Exception e) {
			 e.printStackTrace();
		}
		 return response.toString();
	}

}
