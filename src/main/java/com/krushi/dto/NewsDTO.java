package com.krushi.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "news")
public class NewsDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9097705107462293499L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	 @Column(name = "title")
	 private String title;
	 
	 @Column(name = "description")
	 private String description;
	 
	 @Column(name = "news_image")
	 private byte[] newsImage;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getNewsImage() {
		return newsImage;
	}

	public void setNewsImage(byte[] newsImage) {
		this.newsImage = newsImage;
	}
	 
	 
}
