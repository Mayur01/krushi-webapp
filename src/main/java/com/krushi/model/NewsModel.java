package com.krushi.model;

import javax.persistence.Column;

public class NewsModel {

	private Integer id;
	 private String title;
	 private String description;
	 private byte[] newsImage;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public byte[] getNewsImage() {
		return newsImage;
	}
	public void setNewsImage(byte[] newsImage) {
		this.newsImage = newsImage;
	}
	 
	 
}
