package com.krushi.model;

public class CropModel {

	
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getSoil() {
		return soil;
	}
	public void setSoil(String soil) {
		this.soil = soil;
	}
	private String season;
	private String soil;
	
	public CropModel() {
		
	}
	
	public CropModel(String name, String season, String soil) {
		this.name = name;
		this.season = season;
		this.soil = soil;
	}
}
