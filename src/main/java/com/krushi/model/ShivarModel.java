package com.krushi.model;

import java.io.Serializable;

public class ShivarModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 819752419005009623L;
	private String area;
	private String waterSource;
	private String soilType;
	private String sinchanType;
	private String sideBusiness;
	private Integer userId;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getWaterSource() {
		return waterSource;
	}
	public void setWaterSource(String waterSource) {
		this.waterSource = waterSource;
	}
	public String getSoilType() {
		return soilType;
	}
	public void setSoilType(String soilType) {
		this.soilType = soilType; 
	}
	public String getSinchanType() {
		return sinchanType;
	}
	public void setSinchanType(String sinchanType) {
		this.sinchanType = sinchanType;
	}
	public String getSideBusiness() {
		return sideBusiness;
	}
	public void setSideBusiness(String sideBusiness) {
		this.sideBusiness = sideBusiness;
	}
	
}
