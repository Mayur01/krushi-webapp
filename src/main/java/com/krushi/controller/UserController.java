package com.krushi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.krushi.model.ResponseModel;
import com.krushi.model.UserModel;
import com.krushi.services.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
	@CrossOrigin(origins = "http://localhost:8100")
	@PostMapping("/login")
	public ResponseEntity<ResponseModel> updateCompanyDetails(@RequestBody UserModel userModel){
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			responseModel = userService.login(userModel);
			return new ResponseEntity<>(responseModel, HttpStatus.OK);
		}catch(Exception e) {
			responseModel.setMessage("Login Failed");
			return new ResponseEntity<>(responseModel, HttpStatus.FORBIDDEN);
		}
	}
	@CrossOrigin(origins = "http://localhost:8100")
	@PostMapping("/register")
	public ResponseEntity<ResponseModel> registerUser(@RequestBody UserModel userModel){
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			responseModel.setObject(userService.register(userModel));
			responseModel.setMessage("Register SuccessFully.");
			responseModel.setStatus(HttpStatus.OK.toString());
			return new ResponseEntity<>(responseModel, HttpStatus.OK);
		}catch(Exception e) {
			responseModel.setMessage("Register Failed");
			return new ResponseEntity<>(responseModel, HttpStatus.FORBIDDEN);
		}
	}
	
	@CrossOrigin(origins = "http://localhost:8100")
	@PostMapping("/forgot")
	public ResponseEntity<ResponseModel> forgotPassward(@RequestBody UserModel userModel){
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			responseModel.setObject(userService.forgotPassword(userModel));
			responseModel.setMessage("Passward changed....");
			responseModel.setStatus(HttpStatus.OK.toString());
			return new ResponseEntity<>(responseModel, HttpStatus.OK);
		}catch(Exception e) {
			responseModel.setMessage("Something is wrong..");
			return new ResponseEntity<>(responseModel, HttpStatus.FORBIDDEN);
		}
	}
	
	@CrossOrigin(origins = "http://localhost:8100")
	@PostMapping("/changePassward")
	public ResponseEntity<ResponseModel> changePassward(@RequestBody UserModel userModel){
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			responseModel.setObject(userService.changePassword(userModel));
			responseModel.setMessage("Passward updated....");
			responseModel.setStatus(HttpStatus.OK.toString());
			return new ResponseEntity<>(responseModel, HttpStatus.OK);
		}catch(Exception e) {
			responseModel.setMessage("Something is wrong ...");
			return new ResponseEntity<>(responseModel, HttpStatus.FORBIDDEN);
		}
	}
	
	
}


