package com.krushi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.krushi.model.MarketModel;
import com.krushi.model.ResponseModel;
import com.krushi.services.MarketService;

@RestController
public class MarketController {

	@Autowired
	private MarketService marketService;
	
	@PostMapping("/market")
	public ResponseEntity<ResponseModel> getMarketRate(@RequestBody MarketModel marketModel){
		ResponseModel responseModel = ResponseModel.getInstance();
		System.out.println("responseModel " + responseModel.getObject());
		try {
			responseModel = marketService.getMarketRate(marketModel);
			return new ResponseEntity<>(responseModel, HttpStatus.OK);
		} catch (Exception e) {
			responseModel.setMessage("Something wrong..");
			return new ResponseEntity<>(responseModel,HttpStatus.FORBIDDEN);
		}
	}
}
