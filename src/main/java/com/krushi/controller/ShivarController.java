package com.krushi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.krushi.model.ResponseModel;
import com.krushi.model.ShivarModel;
import com.krushi.services.ShivarService;

@RestController
public class ShivarController {

	@Autowired
	private ShivarService shivarService;
	
	@GetMapping("/getShivar/{userId}")
	ResponseEntity<ResponseModel> getShivarDetails(@PathVariable("userId") Integer userId){
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			responseModel = shivarService.getShivar(userId);
			return new ResponseEntity<>(responseModel,HttpStatus.OK);
		}catch(Exception e) {
			responseModel.setMessage("Fetching Shivar details Failed..");
			return new ResponseEntity<>(responseModel, HttpStatus.FORBIDDEN);
		}
	}
	
	@PostMapping("/saveShivar")
	ResponseEntity<ResponseModel> saveShivarDetails(@RequestBody ShivarModel shivarModel){
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			responseModel = shivarService.saveShivar(shivarModel);
			return new ResponseEntity<>(responseModel,HttpStatus.OK);
		}catch(Exception e) {
			responseModel.setMessage("Shivar details saving failed..");
			return new ResponseEntity<>(responseModel, HttpStatus.FORBIDDEN);
		}
	}
	
}
