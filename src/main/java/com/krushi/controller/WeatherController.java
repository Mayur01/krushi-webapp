package com.krushi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.krushi.model.ResponseModel;
import com.krushi.model.WeatherModel;
import com.krushi.services.WeatherService;

@RestController
public class WeatherController {

	@Autowired
	private WeatherService weatherService;
	
	@PostMapping("/weather")
	public ResponseEntity<ResponseModel> getWeatherDetails(@RequestBody WeatherModel weatherModel){
		ResponseModel responseModel = ResponseModel.getInstance();
		try {
			responseModel = weatherService.weather(weatherModel);
			return new ResponseEntity<>(responseModel,HttpStatus.OK);
		} catch (Exception e) {
			responseModel.setMessage("Weather Fetching failed..");
			return new ResponseEntity<>(responseModel,HttpStatus.FORBIDDEN);
		}
	}
	
}
