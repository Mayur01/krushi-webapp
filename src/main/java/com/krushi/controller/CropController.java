package com.krushi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.krushi.model.CropModel;
import com.krushi.model.ResponseModel;
import com.krushi.services.CropService;

@RestController
public class CropController {

	@Autowired
	private CropService cropService;
	
	@PostMapping("/recommendation")
	public ResponseEntity<ResponseModel> getMarketRate(@RequestBody CropModel cropModel){
		ResponseModel responseModel = ResponseModel.getInstance();
		System.out.println("responseModel " + responseModel.getObject());
		try {
			responseModel = cropService.getCropRecommendation(cropModel);
			return new ResponseEntity<>(responseModel, HttpStatus.OK);
		} catch (Exception e) {
			responseModel.setMessage("Something wrong..");
			return new ResponseEntity<>(responseModel,HttpStatus.FORBIDDEN);
		}
	}
}
