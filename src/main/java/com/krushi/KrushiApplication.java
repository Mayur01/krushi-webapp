package com.krushi;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.krushi.recommendation.RecommendationUtility;


@SpringBootApplication
public class KrushiApplication {

	public static void main(String[] args) throws IOException {
		
		new RecommendationUtility().readFile();
		SpringApplication.run(KrushiApplication.class, args);
	}
	
	/*@Override
    public void run(String... args) Please try the microphone

		new RecommendationUtility().
        

    }*/
}
